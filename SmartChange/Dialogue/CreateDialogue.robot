*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../Resource/system.robot
Resource    ../Navigation/navigationDialogue.robot
Resource    ../Resource/loginUser.robot
Resource    ../Navigation/navigationLogin.robot

*** Tasks ***
Login to Website
    Login User

Create a New Dialogue
    Navigate to Menu Dialogue
    Create Dialogue