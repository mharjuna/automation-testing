*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../Resource/system.robot
Resource    ../Navigation/navigationLogin.robot
Resource    ../Resource/FirstStep.robot
Resource    ../Navigation/navigationInfographics.robot

Suite Setup  Begin Web Test
Suite Teardown    End Web Test

*** Tasks ***
Login to Website
    Login Admin

Navigate to Content Infographics
    Click Menu Content Infographics

Create a New Infographics
    Fill Form Infographics

Check The Infographics
    Check Infographics