*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../Resource/system.robot
Resource    ../Navigation/navigationLogin.robot
Resource    ../Resource/FirstStep.robot
Resource    ../Navigation/navigationArticle.robot

Suite Setup  Begin Web Test
Suite Teardown    End Web Test

*** Tasks ***
Login to Website
    Login Admin

Navigate to Content Article
    Click Menu Content Article

Create a New Article
    Fill Form Article

Check The Article
    Check Article