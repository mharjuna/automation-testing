*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variables.robot


*** Keywords ***
Click Event
    Wait Until Element Is Visible    (//a[@href='/event'])[1]    ${wait}
    Click Element    (//a[@href='/event'])[1]
    Wait Until Element Is Visible    (//small[normalize-space()='Add New'])[1]    ${wait}
    Click Button    (//small[normalize-space()='Add New'])[1]

Fill Form Event
    Wait Until Element Is Visible    (//input[@id='__BVID__250'])[1]    ${wait}
    Input Text    (//input[@id='__BVID__250'])[1]    ${roboText}
    Input Text    (//p[@class='ck-placeholder'])[1]    ${roboDesc}
    Input Text    (//input[@id='__BVID__255'])[1]    ${roboUser}
    Click Button    (//button[normalize-space()='+ Add Speaker'])[1]
    Wait Until Element Is Visible    (//input[@id='__BVID__330'])[1]    ${wait}
    Input Text    (//input[@id='__BVID__330'])[1]    ${roboUser2}
    Input Text    (//input[@type='search'])[1]    ${elemText}
    Press Keys    (//div[@class='vs__selected-options'])[1]    ${pressKey}
    Select From List By Value    (//select[@id='__BVID__263'])[1]    ${false}
    Select From List By Value    (//select[@id='__BVID__269'])[1]    ${false}
    Click Element    (//input[@placeholder='Date'])[1]
    Wait Until Element Is Visible    (//span[contains(text(),'30')])[1]    ${wait}
    Click Element    (//span[contains(text(),'30')])[1]
    Sleep    ${sleep}
    Click Element    (//input[@placeholder='Time'])[1]
    Wait Until Element Is Visible    (//li[contains(text(),'01')])[1]    ${wait}
    Click Element    (//li[contains(text(),'01')])[1]
    Sleep    ${sleep}
    Click Element    (//li[contains(text(),'00')])[2]
    Sleep    ${sleep}
    Wait Until Element Is Visible    (//input[@placeholder='Date'])[1]    ${wait}
    Double Click Element    (//input[@placeholder='Date'])[1]
    Sleep    ${sleep}
    Click Element    (//input[@placeholder='Date'])[2]
    Wait Until Element Is Visible    (//span[contains(text(),'>')])[4]    ${wait}
    Click Element    (//span[contains(text(),'>')])[4]
    Click Element    (//span[@class='cell day'][normalize-space()='31'])[1]
    Sleep    ${sleep}
    Click Element    (//input[@class='display-time form-control is-empty'])[1]
    Wait Until Element Is Visible    (//li[contains(text(),'23')])[3]    ${wait}
    Click Element    (//li[contains(text(),'23')])[3]
    Click Element    (//li[contains(text(),'00')])[4]
    Sleep    ${sleep}
    Double Click Element    (//input[@placeholder='Date'])[2]
    Input Text    (//input[@id='__BVID__283'])[1]    ${roboText}
    Input Text    (//input[@id='__BVID__289'])[1]    ${dummyUrl}
    Input Text    (//input[@id='__BVID__294'])[1]    ${ytURL}
    Choose File    ${eventFile}    ${pdf}
    Choose File    ${eventImg}    ${img}
    Scroll Element Into View    (//button[normalize-space()='ID'])[1]
    Click Element    (//button[normalize-space()='ID'])[1]
    Wait Until Element Is Visible    (//input[@id='__BVID__306'])[1]    ${wait}
    Input Text    (//input[@id='__BVID__306'])[1]    ${roboText}
    Input Text    (//div[@aria-label='Rich Text Editor, main'])[2]    ${roboDesc}
    Sleep    ${sleep}
    Click Button    (//button[normalize-space()='Submit'])[1]

Check Event
    Execute Javascript                          window.open('https://smartchange.co.id/events')
    Sleep    ${sleep}
    Switch Window                               locator=New
    Page Should Contain                         ${roboText}
    Wait Until Element Is Visible               (//img)[14]    ${wait}
    Scroll Element Into View                    (//img)[14]
    Click Element                               (//img)[8]
    Sleep    ${sleep}
