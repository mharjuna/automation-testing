*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variables.robot

*** Keywords ***
User Register as a Talent
    Wait Until Element Is Visible    (//span[normalize-space()='Talents'])[1]    ${wait}
    Click Element    (//span[normalize-space()='Talents'])[1]
    Wait Until Element Is Visible    (//button[normalize-space()='Become a Talent'])[1]    ${wait}
    Scroll Element Into View    (//button[normalize-space()='Become a Talent'])[1]
    Click Element    (//button[normalize-space()='Become a Talent'])[1]
    Wait Until Element Is Visible    (//input[@placeholder='choose your role'])[1]    ${wait}
    Input Text    (//input[@placeholder='choose your role'])[1]    Back
    Press Keys    (//div[@class='vs__selected-options'])[1]    ${pressKey}
    Input Text    (//input[@placeholder='choose your industry'])[1]    Info
    Press Keys    (//div[@class='vs__selected-options'])[2]    ${pressKey}
    Input Text    (//input[@placeholder='(eg: Web Development)'])[1]    QA Engineer
    Input Text    (//div[@aria-label='Rich Text Editor, main'])[1]    ${roboDesc}
    Choose File    (//input[@type='file'])[2]    ${filePdf}
    Choose File    (//input[@type='file'])[3]    ${filePdf}
    Click Element    //label[normalize-space()='Show on Talents Page']
    Checkbox Should Be Selected    id=show-talent-page                                                          
    Checkbox Should Not Be Selected    id=show-profile
    Click Button    (//button[normalize-space()='Submit'])[1]
    Wait Until Element Is Visible    (//button[normalize-space()='OK'])[1]    ${wait}
    Sleep   ${sleep}
    Click Element    (//button[normalize-space()='OK'])[1]

Admin Verify Talents
    Execute Javascript    window.open('https://admin-smartchange.deggan.com/login')
    Switch Window    locator=New
    Wait Until Element Is Visible    id=login-email    ${wait}
    Input Text        id=login-email    admin@smartchange.com
    Input Password    id=login-password    password
    Click Element    //button[contains(text(),'Log In')]
    Wait Until Element Is Visible    //*[@id="main-menu-navigation"]/li[9]/a    ${wait}
    Click Element    //*[@id="main-menu-navigation"]/li[9]/a
    Click Element    (//a[normalize-space()='List'])[1]
    Wait Until Element Is Visible    //tbody/tr[1]    ${wait}
    Click Element    (//a[contains(text(),'Verify')])[1]
    Wait Until Element Is Visible    (//div[@class='row'])[1]    ${wait}
    Sleep   ${sleep}
    Click Element    (//button[normalize-space()='Accept'])[1]
    Wait Until Element Is Visible    (//button[normalize-space()="Yes, I'm Sure"])[1]    ${wait}
    Click Element    (//button[normalize-space()="Yes, I'm Sure"])[1]
    Sleep   ${sleep}
    Wait Until Element Is Visible    (//button[normalize-space()='Verification'])[1]    ${wait}
    Click Element    (//button[normalize-space()='Verification'])[1]
    Sleep   ${sleep}