*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variables.robot

*** Keywords ***
User Report Profile
    Wait Until Element Is Visible    (//i[@class='far fa-user-circle profile-wrapper'])[1]    ${wait}
    Click Element    (//i[@class='far fa-user-circle profile-wrapper'])[1]
    Wait Until Element Is Visible    (//li[@class='user-menu__item'])[1]    ${wait}
    Click Element    (//li[@class='user-menu__item'])[1]
    Sleep    ${sleep}
    Double Click Element    (//a[normalize-space()='Networking'])[1]
    Input Text    (//input[@placeholder='Find people or industries...'])[1]    testing
    Wait Until Element Contains    (//h6[normalize-space()='testing'])[1]    testing
    Click Element    (//h6[normalize-space()='testing'])[1]
    Wait Until Element Contains    (//h6[normalize-space()='testing'])[1]    testing
    Wait Until Element Is Visible    (//button[normalize-space()='Report'])[1]    ${wait}
    Click Button    (//button[normalize-space()='Report'])[1]   
    Wait Until Element Is Visible    (//div[@class='p-4'])[1]    ${wait}
    Click Element    (//label[normalize-space()='Inappropriate profile'])[1]
    Input Text    (//textarea)[1]    Report User Profile Using Robot Framework
    Choose File    (//input[@type='file'])[1]    ${fileImg}
    Click Button    (//button[normalize-space()='Submit'])[1]
    Wait Until Element Contains    (//div[@id='swal2-html-container'])[1]    Success to Submit Report User
    Sleep    ${sleep}
    Click Button    (//button[normalize-space()='OK'])[1]

Admin Verify Report Profile
    Execute Javascript    window.open('https://admin-smartchange.deggan.com/login')
    Sleep    ${sleep}
    Switch Window    locator=New
    Wait Until Element Is Visible    id=login-email    ${wait}
    Input Text    id=login-email    admin@smartchange.com
    Input Password    id=login-password    password
    Click Element    //button[contains(text(),'Log In')]
    Wait Until Element Is Visible    (//a[normalize-space()='Moderation'])[1]    ${wait}
    Click Element    (//a[normalize-space()='Moderation'])[1]
    Sleep    ${sleep}
    Click Element    (//a[normalize-space()='Profile'])[1]
    Wait Until Element Is Visible    (//tr[@role='row'])[7]    ${wait}
    Click Element    (//a[contains(text(),'Detail')])[6]
    Sleep    ${sleep}
    Click Button    (//button[normalize-space()='Action'])[1]
    Wait Until Element Is Visible    (//span[normalize-space()='Close case (not banning)'])[1]    ${wait}
    Click Element    (//span[normalize-space()='Close case (not banning)'])[1]
    Sleep    ${sleep}
    Click Button    (//button[normalize-space()='Submit'])[1]
    Wait Until Element Contains    (//h2[normalize-space()='Are you sure to submit?'])[1]    Are you sure to submit?
    Click Button    (//button[normalize-space()='Yes, accept it!'])[1]
    Sleep    ${sleep}