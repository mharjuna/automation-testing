*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variables.robot

*** Keywords ***
Before Adding Bad Word
    Wait Until Element Is Visible    (//i[@class='far fa-user-circle profile-wrapper'])[1]    ${wait}
    Click Element    (//i[@class='far fa-user-circle profile-wrapper'])[1]
    Wait Until Element Is Visible    (//li[@class='user-menu__item'])[1]    ${wait}
    Click Element    (//li[@class='user-menu__item'])[1]
    Sleep    ${sleep}
    Double Click Element    (//a[normalize-space()='Networking'])[1]
    Input Text    (//input[@placeholder='Find people or industries...'])[1]    testing
    Wait Until Element Contains    (//h6[normalize-space()='testing'])[1]    testing
    Click Element    (//h6[normalize-space()='testing'])[1]
    Wait Until Element Contains    (//h6[normalize-space()='testing'])[1]    testing
    Wait Until Element Is Visible    (//a[normalize-space()='Send Message'])[1]    ${wait}
    Click Element    (//a[normalize-space()='Send Message'])[1]
    Wait Until Element Is Visible    (//span[@class='d-none d-lg-block'])[1]    ${wait}
    Scroll Element Into View    (//span[@class='d-none d-lg-block'])[1]
    Input Text    (//input[@placeholder='Type your message'])[1]    Spongebob
    Click Element    (//span[@class='d-none d-lg-block'])[1]
    Sleep    ${sleep}
    Reload Page
    Sleep    ${sleep}

Add The Bad Word and Checking 
    Execute Javascript    window.open('https://admin-smartchange.deggan.com/login')
    Sleep    ${sleep}
    Switch Window    locator=New
    Wait Until Element Is Visible    id=login-email    ${wait}
    Input Text    id=login-email    admin@smartchange.com
    Input Password    id=login-password    password
    Click Element    //button[contains(text(),'Log In')]
    Wait Until Element Is Visible    (//a[normalize-space()='Settings'])[1]    ${wait}
    Click Element    (//a[normalize-space()='Settings'])[1]
    Wait Until Element Is Visible    (//a[normalize-space()='Bad Word'])[1]    ${wait}
    Click Element    (//a[normalize-space()='Bad Word'])[1]
    Wait Until Element Is Visible    (//small[normalize-space()='Add New'])[1]     ${wait}
    Click Element    (//small[normalize-space()='Add New'])[1]
    Wait Until Element Is Visible    //*[@id="__BVID__77"]    ${wait}
    Input Text    //*[@id="__BVID__77"]    Spongebob
    Sleep    ${sleep}
    Click Button    (//button[normalize-space()='Submit'])[1]
    Sleep    ${sleep}
    Page Should Contain    Successfuly added badword 
    Sleep    ${sleep}
    Click Button    (//button[normalize-space()='OK'])[1]
    Sleep    ${sleep}
    Switch Window    locator=Main
    Reload Page
    Sleep    ${sleep}