*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variables.robot

*** Keywords ***
User Report Message
    Wait Until Element Is Visible    (//i[@class='far fa-user-circle profile-wrapper'])[1]    ${wait}
    Click Element    (//i[@class='far fa-user-circle profile-wrapper'])[1]
    Wait Until Element Is Visible    (//li[@class='user-menu__item'])[1]    ${wait}
    Click Element    (//li[@class='user-menu__item'])[1]
    Sleep    ${sleep}
    Double Click Element    (//a[normalize-space()='Networking'])[1]
    Input Text    (//input[@placeholder='Find people or industries...'])[1]    testing
    Wait Until Element Contains    (//h6[normalize-space()='testing'])[1]    testing
    Click Element    (//h6[normalize-space()='testing'])[1]
    Wait Until Element Contains    (//h6[normalize-space()='testing'])[1]    testing
    Wait Until Element Is Visible    (//a[normalize-space()='Send Message'])[1]    ${wait}
    Click Element    (//a[normalize-space()='Send Message'])[1]
    Wait Until Element Is Visible    (//span[@class='d-none d-lg-block'])[1]    ${wait}
    Scroll Element Into View    (//span[@class='d-none d-lg-block'])[1]
    Input Text    (//input[@placeholder='Type your message'])[1]    Testing Send Message Using Robot Framework
    Click Element    (//span[@class='d-none d-lg-block'])[1]
    Sleep    ${sleep}
    Click Button    (//button[normalize-space()='Report'])[1]
    Page Should Contain    Report this message?
    Click Element    (//label[normalize-space()='Inappropriate content'])[1]
    Input Text    (//textarea)[1]    Description Report Message User Using Robot Framework
    Choose File    (//input[@type='file'])[1]    ${fileImg}
    Click Button    (//button[normalize-space()='Submit'])[1]
    Page Should Contain    Success to Submit Report User
    Sleep    ${sleep}
    Click Button    (//button[normalize-space()='OK'])[1]

Admin Verify Report Message
    Execute Javascript    window.open('https://admin-smartchange.deggan.com/login')
    Sleep    ${sleep}
    Switch Window    locator=New
    Wait Until Element Is Visible    id=login-email    ${wait}
    Input Text    id=login-email    admin@smartchange.com
    Input Password    id=login-password    password
    Click Element    //button[contains(text(),'Log In')]
    Wait Until Element Is Visible    (//a[normalize-space()='Moderation'])[1]    ${wait}
    Click Element    (//a[normalize-space()='Moderation'])[1]
    Sleep    ${sleep}
    Click Element    (//a[normalize-space()='Message'])[1]
    Wait Until Element Is Visible    (//tr[@role='row'])[6]    ${wait}
    Click Element    (//a[contains(text(),'Detail')])[5]
    Wait Until Element Is Visible    (//tr[@role='row'])[2]    ${wait}
    Click Element    (//a[contains(text(),'Review')])[1] 
    Sleep    ${sleep}
    Click Element    (//a[normalize-space()='Back'])[1]
    Wait Until Element Is Visible    (//button[normalize-space()='Action'])[1]    ${wait}
    Click Element    (//button[normalize-space()='Action'])[1]
    Page Should Contain    Actions Taken ?
    Click Element    (//label[@for='radio-group-1_BV_option_2'])[1]
    Click Element    (//button[normalize-space()='Submit'])[1]
    Page Should Contain Button    (//button[normalize-space()='Yes, accept it!'])[1]
    Click Button    (//button[normalize-space()='Yes, accept it!'])[1]
    Sleep    ${sleep}