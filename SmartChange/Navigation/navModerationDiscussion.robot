*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variables.robot

*** Keywords ***
User Report Discussion Online Learning
    Wait Until Element Is Visible    (//span[@class='menu-text'][normalize-space()='Online Learning'])[1]    ${wait}
    Click Element    (//span[@class='menu-text'][normalize-space()='Online Learning'])[1]
    Sleep    ${sleep}
    Wait Until Element Is Visible    (//img)[19]    ${wait}
    Scroll Element Into View    (//a[normalize-space()='Prev'])[1]
    Click Element    (//img)[19]
    Wait Until Element Is Visible    (//a[normalize-space()='Disscussion'])[1]    ${wait}
    Click Element    (//a[normalize-space()='Disscussion'])[1]
    Wait Until Element Is Visible    (//button[normalize-space()='Send'])[1]    ${wait}
    Scroll Element Into View    (//button[normalize-space()='Send'])[1]
    Click Element    (//img[@class='ml-2'])[2]
    Page Should Contain    Are you sure you want to report this comment?
    Click Button    (//button[normalize-space()="Yes, I'm Sure"])[1]
    Sleep    ${sleep}
    Page Should Contain    Successfully reported this comment
    Click Button    (//button[normalize-space()='OK'])[1]

Admin Verify Report Discussion
    Execute Javascript    window.open('https://admin-smartchange.deggan.com/login')
    Sleep    ${sleep}
    Switch Window    locator=New
    Wait Until Element Is Visible    id=login-email    ${wait}
    Input Text    id=login-email    admin@smartchange.com
    Input Password    id=login-password    password
    Click Element    //button[contains(text(),'Log In')]
    Wait Until Element Is Visible    (//a[normalize-space()='Moderation'])[1]    ${wait}
    Click Element    (//a[normalize-space()='Moderation'])[1]
    Sleep    ${sleep}
    Click Element    (//a[normalize-space()='Discussion'])[1]
    Wait Until Element Is Visible    (//tr[@role='row'])[2]    ${wait}
    Click Element    //tbody/tr[1]/td[5]/a[1]
    Sleep    ${sleep}
    Wait Until Element Is Visible    (//button[normalize-space()='Delete'])[1]    ${wait}
    Click Button    (//button[normalize-space()='Delete'])[1]
    Page Should Contain Button    (//button[normalize-space()='Yes, delete it!'])[1]
    Click Button    (//button[normalize-space()='Yes, delete it!'])[1]
    Page Should Contain Element   (//div[@id='swal2-html-container'])[1]
    Sleep    ${sleep}
    Sleep    ${sleep}
    Switch Window    locator=Main
    Sleep    ${sleep}
    Reload Page
    Wait Until Element Is Enabled    (//a[normalize-space()='Disscussion'])[1]    ${wait}0
    Click Element    (//a[normalize-space()='Disscussion'])[1]
    Wait Until Element Is Visible    (//button[normalize-space()='Send'])[1]    ${wait}
    Scroll Element Into View    (//button[normalize-space()='Send'])[1]
    Sleep    ${sleep}