*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variables.robot

*** Keywords ***
Navigate to Menu Online Learning Course
    Wait Until Element Is Visible    (//a[@class='nuxt-link-exact-active nuxt-link-active docs-creator'][normalize-space()='Online Learning'])[1]    ${wait}
    Click Element    (//a[@class='nuxt-link-exact-active nuxt-link-active docs-creator'][normalize-space()='Online Learning'])[1]
    Click Element    (//a[normalize-space()='Course'])[1]
    Click Button     (//a[@class='btn btn-primary docs-creator'])[1]

Create a New Course
    #Form Description
    Wait Until Element Is Visible    (//input[@id='__BVID__75'])[1]    ${wait}
    Input Text    (//input[@id='__BVID__75'])[1]    ${roboText}
    Input Text    (//input[@type='search'])[1]    ${elemText3}
    Press Keys    (//div[@class='vs__selected-options'])[1]    ${pressKey}
    Input Text    (//input[@type='search'])[2]    ${elemText4}
    Press Keys    (//div[@class='vs__selected-options'])[1]    ${pressKey}
    Select From List By Value    (//select[@id='__BVID__89'])[1]    ${false}
    Select From List By Value    (//select[@id='__BVID__92'])[1]    ${false}
    Input Text    (//p)[2]    ${roboDesc}
    Choose File    ${fileCourse}    ${pdf}
    Input Text    (//input[@id='__BVID__114'])[1]    ${ytURL}
    Choose File    ${imgCourse}    ${img}
    Scroll Element Into View    (//button[normalize-space()='ID'])[1]
    Click Element    (//button[normalize-space()='ID'])[1]
    Wait Until Element Is Visible    (//input[@id='__BVID__119'])[1]    ${wait}
    Input Text    (//input[@id='__BVID__119'])[1]    ${roboText}
    Input Text    (//p)[3]    ${roboDesc}
    Click Button    (//button[normalize-space()='Next'])[1]
    
    #Form Curriculum
    Wait Until Element Is Visible    (//input[@id='__BVID__417'])[1]    ${wait}
    Input Text    (//input[@id='__BVID__417'])[1]    ${roboText}
    Input Text    (//input[@type='search'])[3]    ${elemText4}
    Press Keys    (//div[@class='vs__selected-options'])[1]    ${pressKey}
    Choose File    ${fileCurric}    ${pdf}
    Input Text    (//input[@id='__BVID__436'])[1]    ${ytURL}
    Input Text    (//p[@class='ck-placeholder'])[1]    ${roboDesc}
    
    #Add Section
    Click Button    (//button[normalize-space()='+ Add Section'])[1]
    Wait Until Element Is Visible    (//input[@id='__BVID__475'])[1]    ${wait}
    Input Text    (//input[@id='__BVID__475'])[1]    ${roboText}
    Input Text    (//input[@type='search'])[4]    ${elemText4}
    Press Keys    (//div[@class='vs__selected-options'])[1]    ${pressKey}
    Choose File    ${fileCurric2}    ${pdf}
    Input Text    (//input[@id='__BVID__494'])[1]    ${ytURL}
    Input Text    (//p[@class='ck-placeholder'])[2]    ${roboDesc}
    Click Button    (//button[normalize-space()='Submit'])[1]

Verify Online Learning
        Execute Javascript    window.open('https://smartchange.deggan.com/')
        Sleep    ${sleep}
        Switch Window    locator=New
        Wait Until Element Is Visible    (//span[@class='menu-text'][normalize-space()='Online Learning'])[1]    ${wait}
        Click Element    (//span[@class='menu-text'][normalize-space()='Online Learning'])[1]
        Page Should Contain    Create New Online Learning Using Robot Framework
        Wait Until Element Is Visible    (//button[normalize-space()='Sign In to Become a Lecturer'])[1]    ${wait}
        Scroll Element Into View    (//button[normalize-space()='Sign In to Become a Lecturer'])[1]
        Sleep    ${sleep}
        Click Element    (//img[@class='img-cover'])[1]
        Sleep    ${sleep}