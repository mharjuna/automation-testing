*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variables.robot

*** Keywords ***
User Report Topics Dialogue
    Wait Until Element Is Visible    (//span[normalize-space()='Dialogues'])[1]    ${wait}
    Click Element    (//span[normalize-space()='Dialogues'])[1]
    Wait Until Element Is Enabled    (//div[@class='blog-3 row custom-box-shadow'])[1]    ${wait}
    Sleep    ${sleep}
    Scroll Element Into View    (//a[normalize-space()='Next'])[1]
    Click Element    (//a[normalize-space()='Next'])[1]
    Wait Until Element Is Visible    (//a[normalize-space()='**** ****** *****'])[1]    ${wait}
    Scroll Element Into View    (//a[normalize-space()='**** ****** *****'])[1]
    Click Link    (//a[normalize-space()='**** ****** *****'])[1]
    Wait Until Element Is Visible    (//button[@class='btn btn-outline-primary'])[1]    ${wait}
    Click Element    (//button[@class='btn btn-outline-primary'])[1]
    Click Button    (//button[normalize-space()="Yes, I'm Sure"])[1]
    Page Should Contain    Successfully reported this topic
    Sleep    ${sleep}
    Click Button    (//button[normalize-space()='OK'])[1]

Admin Verify Report Dialogue Topics
    Execute Javascript    window.open('https://admin-smartchange.deggan.com/login')
    Sleep    ${sleep}
    Switch Window    locator=New
    Wait Until Element Is Visible    id=login-email    ${wait}
    Input Text    id=login-email    admin@smartchange.com
    Input Password    id=login-password    password
    Click Element    //button[contains(text(),'Log In')]
    Wait Until Element Is Visible    (//a[normalize-space()='Moderation'])[1]    ${wait}
    Click Element    (//a[normalize-space()='Moderation'])[1]
    Sleep    ${sleep}
    Click Element    (//a[normalize-space()='Dialogue'])[1]
    Wait Until Element Is Visible    (//tr[@role='row'])[2]    ${wait}
    Click Element    (//a[contains(text(),'Review')])[3]
    Sleep    ${sleep}
    Click Button    (//button[normalize-space()='Delete'])[1]
    Sleep    ${sleep}
    Click Button    (//button[normalize-space()='Yes, delete it!'])[1]
    Page Should Contain    Delete
    Sleep    ${sleep}
    Click Button    (//button[normalize-space()='OK'])[1]
    Sleep    ${sleep}
    Reload Page
    Wait Until Element Is Visible    (//tr[@role='row'])[2]    ${wait}
    Sleep    ${sleep}
    Close Browser