*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variables.robot

*** Keywords ***
Navigate to Menu Dialogue
    Wait Until Element Is Visible    (//span[normalize-space()='Dialogues'])[1]    ${wait}
    Click Element    (//span[normalize-space()='Dialogues'])[1]
Create Dialogue
    Wait Until Page Contains Element    (//div[@class='row row-cols-1 no-gutters pt-3'])[1]    ${wait}
    Wait Until Element Is Visible    (//a[normalize-space()='Create New Dialogue'])[1]    ${wait}
    Click Element    (//a[normalize-space()='Create New Dialogue'])[1]
    Wait Until Element Is Visible    (//input[@placeholder='Title'])[1]    ${wait}
    Input Text    (//input[@placeholder='Title'])[1]    ${roboText}
    Input Text    (//input[@placeholder='Tags'])[1]    ${robo}
    Press Keys    (//input[@placeholder='Tags'])[1]    ${pressKey}
    Wait Until Element Is Visible    (//div[@aria-label='Rich Text Editor, main'])[1]    ${wait}
    Input Text    (//div[@aria-label='Rich Text Editor, main'])[1]    ${roboDesc}
    Sleep    ${sleep}
    Click Button    (//button[normalize-space()='Save'])[1]
    Sleep    ${sleep}
    Wait Until Location Is    https://smartchange.deggan.com/dialogues/mine    ${wait}
    Sleep    ${sleep}