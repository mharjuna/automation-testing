*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variables.robot

*** Keywords ***

Login Admin
    Wait Until Element Is Visible    (//input[@id='login-email'])[1]    ${wait}
    Input Text     (//input[@id='login-email'])[1]    admin@smartchange.com
    Input Password    (//input[@id='login-password'])[1]    deggan456
    Click Button    (//button[normalize-space()='Log In'])[1]

Login User
    Wait Until Element Is Visible    (//button[normalize-space()='Login'])[1]    ${wait}
    Click Element    (//button[normalize-space()='Login'])[1]
    Wait Until Element Is Visible    (//input[@placeholder='john@example.com'])[1]    ${wait}
    Input Text    (//input[@placeholder='john@example.com'])[1]    asd@gmail.com
    Input Password    (//input[@placeholder='············'])[1]    1
    Click Button    (//button[normalize-space()='LOGIN'])[1]