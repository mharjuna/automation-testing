*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variables.robot

*** Keywords ***
Click Menu Content Infographics
    Wait Until Element Is Visible    (//a[normalize-space()='Content'])[1]    ${wait}
    Click Element    (//a[normalize-space()='Content'])[1]
    Sleep    ${sleep}
    Click Element    (//a[normalize-space()='Infographics'])[1]

Fill Form Infographics
    Click Button    (//small[normalize-space()='Add New'])[1]
    Wait Until Element Is Visible    (//input[@id='__BVID__121'])[1]    ${wait}
    Input Text    (//input[@id='__BVID__121'])[1]    ${roboText}
    Input Text    (//input[@placeholder='Tags'])[1]    ${robo}
    Input Text    (//input[@id='__BVID__126'])[1]    ${roboText}
    Choose File    ${infoFile}    ${img}
    
    #Add New Infographics
    Click Button    (//button[normalize-space()='+ Add New'])[1]
    Wait Until Element Is Visible    (//input[@id='__BVID__140'])[1]    ${wait}
    Input Text    (//input[@id='__BVID__140'])[1]    ${roboText}
    Choose File    ${infoFile2}    ${img}
    Scroll Element Into View    (//button[normalize-space()='ID'])[1]
    Click Element    (//button[normalize-space()='ID'])[1]
    Wait Until Element Is Visible    (//input[@id='__BVID__129'])[1]    ${wait}
    Input Text    (//input[@id='__BVID__129'])[1]    ${roboText}
    Input Text    (//input[@id='__BVID__132'])[1]    ${roboText}
    Input Text    (//input[@id='__BVID__146'])[1]    ${roboText}
    Click Button    (//button[normalize-space()='Submit'])[1]

Check Infographics
    Execute Javascript    window.open('https://smartchange.deggan.com/')
    Sleep    ${sleep}
    Switch Window    locator=New
    Wait Until Element Is Visible    (//span[normalize-space()='Contents'])[1]    ${wait}
    Click Element    (//span[normalize-space()='Contents'])[1]
    Wait Until Element Is Visible    (//li)[6]    ${wait}
    Click Element    (//li)[6]
    Mouse Over    (//span[@class='menu-text'][normalize-space()='Online Learning'])[1]
    Page Should Contain Element    (//a[normalize-space()='Create New Content Infographic...'])[1]
    Sleep    ${sleep}
    Wait Until Element Is Visible    (//img)[3]    ${wait}
    Click Element    (//img)[3]
    Sleep    ${wait}

