*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variables.robot

*** Keywords ***
User Register as a Lecturer
    Wait Until Element Is Visible    (//span[@class='menu-text'][normalize-space()='Online Learning'])[1]    ${wait}
    Click Element    (//span[@class='menu-text'][normalize-space()='Online Learning'])[1]
    Wait Until Element Is Visible    (//a[normalize-space()='Become a Lecturer'])[1]    ${wait}
    Scroll Element Into View    (//a[normalize-space()='Become a Lecturer'])[1]
    Click Element    (//a[normalize-space()='Become a Lecturer'])[1]
    Wait Until Element Is Visible    (//div[@aria-label='Rich Text Editor, main'])[1]    ${wait}
    Input Text    (//div[@aria-label='Rich Text Editor, main'])[1]    ${roboDesc}
    Choose File    (//input[@type='file'])[2]    ${fileImg}
    Choose File    (//input[@type='file'])[3]    ${fileImg}
    Choose File    (//input[@accept='.pdf'])[1]    ${filePdf}
    Click Element    (//button[normalize-space()='Submit'])[1]
    Wait Until Element Is Visible    (//p[@class='text-success'])[1]    ${wait}
    Page Should Contain Element    (//p[@class='text-success'])[1]
    Sleep    ${sleep}
    Click Element    (//button[normalize-space()='OK'])[1]
    Sleep   ${sleep}

Admin Verify Lecturer
    Execute Javascript    window.open('https://admin-smartchange.deggan.com/login')
    Switch Window    locator=New
    Wait Until Element Is Visible    id=login-email    ${wait}
    Input Text    id=login-email    admin@smartchange.com
    Input Password    id=login-password    password
    Click Element    //button[contains(text(),'Log In')]
    Wait Until Element Is Visible    (//a[normalize-space()='Lecturer'])[1]    ${wait}
    Click Element    (//a[normalize-space()='Lecturer'])[1]
    Wait Until Element Is Visible    (//tr[@role='row'])[2]    ${wait}
    Click Element    (//button[@type='submit'][normalize-space()='Verfiy'])[1]
    Click Element    (//button[normalize-space()='Accept'])[1]
    Click Element    (//button[normalize-space()="Yes, I'm Sure"])[1]
    Wait Until Element Is Visible    (//div[@id='swal2-html-container'])[1]    ${wait}
    Page Should Contain Button    (//button[normalize-space()='OK'])[1]
    Sleep    ${sleep}
    Click Element    (//button[normalize-space()='OK'])[1]                                 
    Sleep   ${sleep}