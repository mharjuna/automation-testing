*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variables.robot


*** Keywords ***
Click Menu Content Article
    Wait Until Element Is Visible    (//a[normalize-space()='Content'])[1]    ${wait}
    Click Element    (//a[normalize-space()='Articles'])[1]

Fill Form Article
    Wait Until Element Is Visible    (//a[@class='btn btn-primary docs-creator'])[1]    ${wait}
    Click Button    (//a[@class='btn btn-primary docs-creator'])[1]
    Wait Until Element Is Visible    (//input[@id='__BVID__97'])[1]    ${wait}
    Input Text    (//input[@id='__BVID__97'])[1]    ${roboText}
    Input Text    (//input[@type='search'])[1]    ${elemText2}
    Press Keys    (//div[@class='vs__selected-options'])[1]    ${pressKey}
    Input Text    (//input[@placeholder='Tags'])[1]    ${robo}
    Input Text    (//p)[1]    ${roboDesc}
    Choose File    ${articleFile}    ${pdf}
    Choose File    ${articleImg}    ${img}
    Scroll Element Into View    (//button[normalize-space()='ID'])[1]
    Click Element    (//button[normalize-space()='ID'])[1]
    Wait Until Element Is Visible    (//input[@id='__BVID__113'])[1]    ${wait}
    Input Text    (//input[@id='__BVID__113'])[1]    ${roboText}
    Input Text    (//p)[2]    ${roboDesc}
    Sleep    ${sleep}
    Click Button    (//button[normalize-space()='Submit'])[1]

Check Article
        Execute Javascript    window.open('https://smartchange.deggan.com/')
        Sleep    ${sleep}
        Switch Window    locator=New
        Wait Until Element Is Visible    (//span[normalize-space()='Contents'])[1]    ${wait}
        Click Element    (//span[normalize-space()='Contents'])[1]
        Wait Until Element Is Visible    (//li)[9]    ${wait}
        Click Element    (//li)[9]
        Mouse Out    (//li)[9]
        Page Should Contain    ${roboText}
        Sleep    ${sleep}
        Wait Until Element Is Visible    (//img[@class='img-cover'])[1]    ${wait}
        Click Element    (//img[@class='img-cover'])[1]
        Sleep    ${sleep}