*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../Resource/system.robot
Resource    ../Resource/loginUser.robot
Resource    ../Navigation/NavModerationDialogue.robot
Resource    ../Navigation/navigationLogin.robot

Suite Setup    Begin Web Test
Suite Teardown    End Web Test

*** Keywords ***
Login to Website
    Login User

User Report Dialogue
    User Report Topics Dialogue
    Admin Verify Report Dialogue Topics