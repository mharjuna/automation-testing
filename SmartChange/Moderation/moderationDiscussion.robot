*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../Resource/system.robot
Resource    ../Navigation/navModerationDiscussion.robot
Resource    ../Navigation/navigationLogin.robot
Resource    ../Resource/loginUser.robot

Suite Setup    Begin Web Test
Suite Teardown    End Web Test

*** Keywords ***
Login to Website
    Login User

User Report Discussion
    User Report Discussion Online Learning
    Admin Verify Report Discussion