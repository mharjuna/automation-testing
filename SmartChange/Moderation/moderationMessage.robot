*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../Resource/loginUser.robot
Resource    ../Resource/system.robot
Resource    ../Navigation/navModerationMessage.robot
Resource    ../Navigation/navigationLogin.robot

Suite Setup    Begin Web Test
Suite Teardown    End Web Test

*** Keywords ***
Login to Website
    Login User

User Report Message
    User Report Message
    Admin Verify Report Message