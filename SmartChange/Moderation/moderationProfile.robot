*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../Resource/system.robot
Resource    ../Navigation/navModerationProfile.robot
Resource    ../Resource/loginUser.robot
Resource    ../Navigation/navigationLogin.robot

Suite Setup    Begin Web Test
Suite Teardown    End Web Test

*** Tasks ***
Login to Website
    Login User

User Report Profile
    User Report Profile
    Admin Verify Report Profile