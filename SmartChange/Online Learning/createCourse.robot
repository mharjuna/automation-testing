*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../Resource/system.robot
Resource    ../Navigation/navigationCourse.robot
Resource    ../Resource/FirstStep.robot
Resource    ../Navigation/navigationLogin.robot

Suite Setup    Begin Web Test
Suite Teardown    End Web Test

*** Tasks ***
Login to Website
    Login Admin

Create a New Course
    Navigate to Menu Online Learning Course
    Create a New Course
    Verify Online Learning