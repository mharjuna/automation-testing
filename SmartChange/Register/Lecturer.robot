*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../Resource/system.robot
Resource    ../Resource/loginUser.robot
Resource    ../Navigation/navRegisterLecturer.robot
Resource    ../Navigation/navigationLogin.robot

Suite Setup    Begin Web Test
Suite Teardown    End Web Test

*** Tasks ***
Login to Website
    Login User

Register Talent
    User Register as a Lecturer
    Admin Verify Lecturer