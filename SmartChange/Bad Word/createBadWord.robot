*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../Resource/system.robot
Resource    ../Navigation/navigationBadWord.robot
Resource    ../Resource/loginUser.robot
Resource    ../Navigation/navigationLogin.robot

Suite Setup    Begin Web Test
Suite Teardown    End Web Test

*** Tasks ***
Login to Website
    Login User

Create a Bad Word and Test The Bad Word
    Before Adding Bad Word
    Add The Bad Word and Checking
    