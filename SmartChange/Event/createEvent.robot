*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../Resource/system.robot
Resource    ../Navigation/navigationLogin.robot
Resource    ../Resource/FirstStep.robot
Resource    ../Navigation/navigationEvent.robot

Suite Setup  Begin Web Test
Suite Teardown    End Web Test

*** Tasks ***
Login to Website
    Login Admin

Navigate to Menu Event
    Click Event

Create a New Event
    Fill Form Event

Check The Event
    Check Event

    