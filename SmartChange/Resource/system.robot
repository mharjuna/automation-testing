*** Settings ***
Library    SeleniumLibrary
Resource   variables.robot
Library    String

*** Keywords ***
Configure Selenium
    Set Selenium Speed    .100 Seconds

Navigate To Website
    Open Browser    ${siteUrl}    ${browser}
    Register Keyword To Run On Failure    NONE
    Maximize Browser Window

Exit Browser
    Close Browser

Generate Random Number
    ${random_number}    Evaluate    random.randint(1000000, 9999999)
    [return]    ${random_number}



