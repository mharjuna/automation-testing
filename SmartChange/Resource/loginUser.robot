*** Settings ***
Library    SeleniumLibrary
Resource    variables.robot

*** Keywords ***
Begin Web Test
    Open Browser     ${url}    ${browser}
    Register Keyword To Run On Failure    ${none}
    Maximize Browser Window

End Web Test
    Close Browser