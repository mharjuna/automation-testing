*** Variables ***
${browser}    chrome
${none}       NONE
${siteUrl}    https://admin.smartchange.co.id/login
${url}        smartchange.co.id
${wait}       100s
${sleep}      5s
${directory}  D:\\Work\\Deggan\\Automation\\automation-testing\\SmartChange\\Screenshot\\
${filePdf}    D:\\Work\\Deggan\\Automation\\automation-testing\\DummyDocs\\dummies.pdf
${fileImg}    D:\\Work\\Deggan\\Automation\\automation-testing\\DummyDocs\\lorem-ipsum.png
${roboText}   This Made With Robot Framework
${robo}       automation
${roboDesc}   Description Made With Robot Framework
${roboUser}   Mr. Robot Framework
${roboUser2}  Ms. Robot Framwork
${elemText}   People
${elemText2}  Blog
${elemText3}  Design
${elemText4}  Adm
${pressKey}   ENTER
${true}       true
${false}      false
${dummyUrl}   https://meet.google.com
${ytURL}      https://www.youtube.com/watch?v=X3eyqaWAhrY
${pdf}        D:\\Work\\Deggan\\Automation\\automation-testing\\DummyDocs\\dummies.pdf
${img}        D:\\Work\\Deggan\\Automation\\automation-testing\\DummyDocs\\lorem-ipsum.png
${eventPath}  D:\\Work\\Deggan\\Automation\\automation-testing\\SmartChange\\Event\\Screenshot
${eventFile}  (//input[@accept='.xlsx,.xls,.doc,.docx,.ppt,.pptx,.txt,.pdf'])[1]
${eventImg}   (//input[@accept='.jpg,.png,.jpeg'])[1]
${articleFile}    (//input[@accept='.pdf'])[1]
${articleImg}     (//input[@accept='.jpg,.png,.jpeg'])[1]
${infoFile}       (//input[@type='file'])[1]
${infoFile2}      (//input[@type='file'])[2]
${fileCourse}     (//input[@type='file'])[2]
${imgCourse}      (//input[@accept='.jpg,.png,.jpeg'])[1]
${fileCurric}     (//input[@type='file'])[6]
${fileCurric2}    (//input[@type='file'])[9]
