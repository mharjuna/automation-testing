*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../../Navigation/navigationLogin.robot
Resource    ../../Resource/roboSetting.robot
Resource    ../../Navigation/navigationDocuments.robot

Suite Setup  Begin Web Test
Suite Teardown    End Web Test  

*** Tasks ***
Login Using User Verifikator
    Login Verifikator

Create New Document
    Navigate to Document Menu
    Create a New Document
    