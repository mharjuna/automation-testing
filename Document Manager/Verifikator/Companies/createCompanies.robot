*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../../Navigation/navigationLogin.robot
Resource    ../../Resource/roboSetting.robot
Resource    ../../Navigation/navigationCompanies.robot

Suite Setup  Begin Web Test
Suite Teardown    End Web Test

*** Tasks ***
Login Using User Verifikator
    Login Verifikator

Create New Companies
    Navigate to Companies Menu
    Create a New Companies

Verify The Companies Data Is Showing
    Verify The Companies