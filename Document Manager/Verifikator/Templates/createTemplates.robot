*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../../Navigation/navigationTemplate.robot
Resource    ../../Navigation/navigationLogin.robot
Resource    ../../Resource/roboSetting.robot

Suite Setup  Begin Web Test
Suite Teardown    End Web Test

*** Tasks ***
Login Using User Verifikator
    Login Verifikator

Create New Template
    Navigate to Templates Menu
    Create a New Templates