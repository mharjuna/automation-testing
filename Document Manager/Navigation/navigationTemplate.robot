*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variable.robot

*** Keywords ***
Navigate to Templates Menu
    Wait Until Element Is Visible  (//span[normalize-space()='Templates'])[1]    ${wait}
    Click Element    (//span[normalize-space()='Templates'])[1]
    Sleep    ${sleep}

Create a New Templates
    Click Button    (//button[normalize-space()='Add New'])[1]
    Input Text    id=title    ${roboTitle}
    Wait Until Element Is Visible    (//div[@role='group'])[3]
    # Select From List By Label    (//div[@role='group'])[3]    nama
    # Select From List By Value    (//div[@role='group'])[3]    1
    Choose File    class=custom-file-input    ${docx}
    Click Button    (//button[normalize-space()='Save'])[1]
