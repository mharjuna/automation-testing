*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../Resource/variable.robot

*** Keywords ***
Login Verifikator
    Wait Until Element Is Visible   ${elemEmail}     ${wait}
    Clear Element Text    ${elemEmail}
    Input Text    ${elemEmail}    ${emailVerifikator}
    Click Button    (//button[normalize-space()='Login'])[1]

Login Teknis
    Wait Until Element Is Visible    ${elemEmail}    ${wait}
    Clear Element Text    ${elemEmail}
    Input Text   ${elemEmail}    ${emailTeknis}
    Click Button     (//button[normalize-space()='Login'])[1]

Login Client
    Wait Until Element Is Visible    ${elemEmail}    ${wait}
    Clear Element Text    ${elemEmail}
    Input Text    ${elemEmail}    ${emailClient}
    Click Button    (//button[normalize-space()='Login'])[1]