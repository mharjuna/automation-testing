*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variable.robot

*** Keywords ***
Navigate to Companies Menu
    Wait Until Element Is Visible    (//span[normalize-space()='Companies'])[1]    ${wait}
    Click Element    (//span[normalize-space()='Companies'])[1]
    Sleep    ${sleep}

Create a New Companies
    Click Button    (//button[normalize-space()='Add New'])[1]
    Input Text    id=company_name    PT.Deggan Technowave
    Input Text    id=address    Depok
    Choose File    class=custom-file-input    ${img}
    Click Element    (//label[normalize-space()='Active'])[1]
    Click Button    (//button[normalize-space()='Save'])[1]
    Sleep    ${sleep}

Verify The Companies
    Wait Until Element Is Visible    (//tbody)[1]    ${wait}
    Click Element    (//button[contains(text(),'›')])[1]
    Sleep    ${sleep}
    Click Element    (//button[contains(text(),'›')])[1]
    Sleep    ${sleep}
    Page Should Contain    PT.Deggan Technowave
    Sleep    ${sleep}
