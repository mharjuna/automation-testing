*** Settings ***
Library    SeleniumLibrary

Resource    ../Resource/variable.robot

*** Keywords ***
Navigate to Document Menu
    Wait Until Element Is Visible    (//a[@href='/documents'])[1]    ${wait}
    Click Element    (//a[@href='/documents'])[1]
    Sleep    ${sleep}

Create a New Document
    Click Button    (//button[normalize-space()='Add New'])[1]
    Wait Until Element Is Visible    (//input[@type='search'])[2]    ${wait}
    Input Text    (//input[@type='search'])[2]    Connor
    Press Keys    (//div[@class='vs__selected-options'])[2]    ${press}
    Input Text    (//input[@type='search'])[3]    Template
    Press Keys    (//div[@class='vs__selected-options'])[3]    ${press}
    Input Text    id=custom-company-name    PT.Deggan Technowave
    Choose File    class=custom-file-input    ${img}
    Click Button     (//button[normalize-space()='Save'])[1]

    
