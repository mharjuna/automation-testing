*** Settings ***
Library    SeleniumLibrary


*** Variables ***
${path}    D:\\Work\\Deggan\\Automation\\automation-testing\\SmartChange\\Screenshot\\

*** Test Cases ***
Moderation Dialogue
        [Documentation]    User Report Dialogue Topics
        Open Browser    https://smartchange.deggan.com    Chrome
        Register Keyword To Run On Failure    NONE
        Maximize Browser Window
        Wait Until Element Is Visible    (//button[normalize-space()='Login'])[1]    10
        Click Element    (//button[normalize-space()='Login'])[1]
        Wait Until Element Is Visible    (//input[@placeholder='john@example.com'])[1]    10
        Input Text    (//input[@placeholder='john@example.com'])[1]    asd3@gmail.com
        Input Password    (//input[@placeholder='············'])[1]    1
        Click Button    (//button[normalize-space()='LOGIN'])[1]
        Wait Until Element Is Visible    (//span[normalize-space()='Dialogues'])[1]    10
        Click Element    (//span[normalize-space()='Dialogues'])[1]
        Wait Until Element Is Enabled    (//div[@class='blog-3 row custom-box-shadow'])[1]    10
        Sleep    1s
        Scroll Element Into View    (//a[normalize-space()='Next'])[1]
        Click Element    (//a[normalize-space()='Next'])[1]
        Wait Until Element Is Visible    (//a[normalize-space()='**** ****** *****'])[1]    10
        Scroll Element Into View    (//a[normalize-space()='**** ****** *****'])[1]
        Click Link    (//a[normalize-space()='**** ****** *****'])[1]
        Capture Page Screenshot    ${path}showDialogueTopic.png
        Wait Until Element Is Visible    (//button[@class='btn btn-outline-primary'])[1]    10
        Click Element    (//button[@class='btn btn-outline-primary'])[1]
        Capture Page Screenshot    ${path}popUpWarningReportDialogue.png
        Click Button    (//button[normalize-space()="Yes, I'm Sure"])[1]
        Page Should Contain    Successfully reported this topic
        Sleep    3s
        Capture Page Screenshot    ${path}successReportTopic.png
        Sleep    1s
        Click Button    (//button[normalize-space()='OK'])[1]

Verify Report Dialogue Topic
        [Documentation]    Admin Verify The Report Dialogue Topic
        Execute Javascript    window.open('https://admin-smartchange.deggan.com/login')
        Sleep    1s
        Switch Window    locator=New
        Wait Until Element Is Visible    id=login-email    10
        Input Text    id=login-email    admin@smartchange.com
        Input Password    id=login-password    password
        Click Element    //button[contains(text(),'Log In')]
        Wait Until Element Is Visible    (//a[normalize-space()='Moderation'])[1]    10
        Click Element    (//a[normalize-space()='Moderation'])[1]
        Sleep    1s
        Click Element    (//a[normalize-space()='Dialogue'])[1]
        Wait Until Element Is Visible    (//tr[@role='row'])[2]    10
        Capture Page Screenshot    ${path}listReportTopic.png
        Click Element    (//a[contains(text(),'Review')])[3]
        Sleep    1s
        Capture Page Screenshot    ${path}reviewReportDialogueTopic.png
        Click Button    (//button[normalize-space()='Delete'])[1]
        Sleep    1s
        Capture Page Screenshot    ${path}popUpWarningDeleteTopic.png
        Click Button    (//button[normalize-space()='Yes, delete it!'])[1]
        Page Should Contain    Delete
        Sleep    1s
        Capture Page Screenshot    ${path}successDeleteTopic.png
        Click Button    (//button[normalize-space()='OK'])[1]
        Sleep    1s
        Reload Page
        Wait Until Element Is Visible    (//tr[@role='row'])[2]    10
        Sleep    3s
        Capture Page Screenshot    ${path}resultDeleteTopic.png
        Close Browser
