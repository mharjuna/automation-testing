*** Settings ***
Library                                                             SeleniumLibrary

*** Variables ***
${filePdf}                                                          D:\\Work\\Deggan\\Automation\\automation-testing\\DummyDocs\\dummies.pdf
${fileImg}                                                          D:\\Work\\Deggan\\Automation\\automation-testing\\DummyDocs\\lorem-ipsum.png
${path}                                                             D:\\Work\\Deggan\\Automation\\automation-testing\\SmartChange\\Screenshot\\

*** Test Cases ***
Register Talent
        [Documentation]                                             User Register Become a Talent on Smart Change
        Open Browser                                                https://smartchange.deggan.com/                                                     Chrome
        Register Keyword To Run On Failure                          NONE
        Maximize Browser Window
        Wait Until Element Is Visible                               (//button[normalize-space()='Login'])[1]                                            10
        Click Element                                               (//button[normalize-space()='Login'])[1]
        Wait Until Element Is Visible                               (//input[@placeholder='john@example.com'])[1]                                       10
        Input Text                                                  (//input[@placeholder='john@example.com'])[1]                                       asd@gmail.com
        Input Password                                              (//input[@placeholder='············'])[1]                                           1
        Click Element                                               (//button[normalize-space()='LOGIN'])[1]
        Wait Until Element Is Visible                               (//span[normalize-space()='Talents'])[1]                                            10
        Click Element                                               (//span[normalize-space()='Talents'])[1]
        Wait Until Element Is Visible                               (//button[normalize-space()='Become a Talent'])[1]                                  10
        Scroll Element Into View                                    (//button[normalize-space()='Become a Talent'])[1]
        Click Element                                               (//button[normalize-space()='Become a Talent'])[1]
        Wait Until Element Is Visible                               (//input[@placeholder='choose your role'])[1]                                       10
        Input Text                                                  (//input[@placeholder='choose your role'])[1]                                       Back
        Press Keys                                                  (//div[@class='vs__selected-options'])[1]                                           ENTER
        Input Text                                                  (//input[@placeholder='choose your industry'])[1]                                   Info
        Press Keys                                                  (//div[@class='vs__selected-options'])[2]                                           ENTER
        Input Text                                                  (//input[@placeholder='(eg: Web Development)'])[1]                                  QA Engineer
        Input Text                                                  (//div[@aria-label='Rich Text Editor, main'])[1]                                    DDescription User Talents
        Choose File                                                 (//input[@type='file'])[2]                                                          ${filePdf}
        Choose File                                                 (//input[@type='file'])[3]                                                          ${filePdf}
        Click Element                                               //label[normalize-space()='Show on Talents Page']
        Checkbox Should Be Selected                                 id=show-talent-page                                                          
        Checkbox Should Not Be Selected                             id=show-profile
        Capture Page Screenshot                                     ${path}formRegisterTalent.jpg
        Click Button                                                (//button[normalize-space()='Submit'])[1]
        Wait Until Element Is Visible                               (//button[normalize-space()='OK'])[1]                                               10
        Sleep   1s
        Capture Page Screenshot                                     ${path}popUpRegisterTalentSuccess.jpg
        Click Element                                               (//button[normalize-space()='OK'])[1]

Verify Talents
        [Documentation]                                             Admin Verify User Registration Talents on Smart Change
        Execute Javascript                                          window.open('https://admin-smartchange.deggan.com/login')
        Switch Window                                               locator=New
        Wait Until Element Is Visible                               id=login-email                                                                      10
        Input Text                                                  id=login-email                                                                      admin@smartchange.com
        Input Password                                              id=login-password                                                                   password
        Click Element                                               //button[contains(text(),'Log In')]
        Wait Until Element Is Visible                               //*[@id="main-menu-navigation"]/li[9]/a                                             10
        Click Element                                               //*[@id="main-menu-navigation"]/li[9]/a
        Click Element                                               (//a[normalize-space()='List'])[1]
        Wait Until Element Is Visible                               //tbody/tr[1]                                                                       10
        Capture Element Screenshot                                  //tbody/tr[1]                                                                       ${path}verifyUserTalent.jpg
        Click Element                                               (//a[contains(text(),'Verify')])[1]
        Wait Until Element Is Visible                               (//div[@class='row'])[1]                                                            10
        Sleep   3s
        Capture Page Screenshot                                     ${path}dataTalentUser.jpg
        Click Element                                               (//button[normalize-space()='Accept'])[1]
        Wait Until Element Is Visible                               (//button[normalize-space()="Yes, I'm Sure"])[1]                                    10
        Click Element                                               (//button[normalize-space()="Yes, I'm Sure"])[1]
        Sleep   1s
        Capture Page Screenshot                                     ${path}successfullyVerifTalents.jpg
        Wait Until Element Is Visible                               (//button[normalize-space()='Verification'])[1]                                     10
        Click Element                                               (//button[normalize-space()='Verification'])[1]
        Sleep   1s
        Capture Page Screenshot                                     ${path}listVerificationTalent.jpg
        Close Browser                                                