*** Settings ***
Library                                                             SeleniumLibrary

*** Variables ***
${filePdf}                                                          D:\\Work\\Deggan\\Automation\\automation-testing\\DummyDocs\\dummies.pdf
${fileImg}                                                          D:\\Work\\Deggan\\Automation\\automation-testing\\DummyDocs\\lorem-ipsum.png
${path}                                                             D:\\Work\\Deggan\\Automation\\automation-testing\\SmartChange\\Screenshot\\


*** Test Cases ***
Register Lecturer
        [Documentation]                                             User Register Lecturer in Smart Change
        Open Browser                                                https://smartchange.deggan.com/                                                     Chrome
        Register Keyword To Run On Failure                          NONE
        Maximize Browser Window
        Wait Until Element Is Visible                               (//button[normalize-space()='Login'])[1]                                            10
        Click Element                                               (//button[normalize-space()='Login'])[1]
        Wait Until Element Is Visible                               (//input[@placeholder='john@example.com'])[1]                                       10
        Input Text                                                  (//input[@placeholder='john@example.com'])[1]                                       asd@gmail.com
        Input Password                                              (//input[@placeholder='············'])[1]                                           1
        Click Element                                               (//button[normalize-space()='LOGIN'])[1]
        Wait Until Element Is Visible                               (//span[@class='menu-text'][normalize-space()='Online Learning'])[1]                10
        Click Element                                               (//span[@class='menu-text'][normalize-space()='Online Learning'])[1]
        Wait Until Element Is Visible                               (//a[normalize-space()='Become a Lecturer'])[1]                                     10
        Scroll Element Into View                                    (//a[normalize-space()='Become a Lecturer'])[1]
        Click Element                                               (//a[normalize-space()='Become a Lecturer'])[1]
        Wait Until Element Is Visible                               (//div[@aria-label='Rich Text Editor, main'])[1]                                    10
        Input Text                                                  (//div[@aria-label='Rich Text Editor, main'])[1]                                    HHallo, saya ingin mendaftar menjadi guru di Smart Change!
        Choose File                                                 (//input[@type='file'])[2]                                                          ${fileImg}
        Choose File                                                 (//input[@type='file'])[3]                                                          ${fileImg}
        Choose File                                                 (//input[@accept='.pdf'])[1]                                                        ${filePdf}
        Capture Page Screenshot                                     ${path}formRegistrerLecturerUser.png
        Click Element                                               (//button[normalize-space()='Submit'])[1]
        Wait Until Element Is Visible                               (//p[@class='text-success'])[1]                                                     10
        Page Should Contain Element                                 (//p[@class='text-success'])[1]
        Sleep    1s
        Capture Page Screenshot                                     ${path}successRegisterLecturer.png
        Click Element                                               (//button[normalize-space()='OK'])[1]
        Sleep   3s
        
Verification Register
        [Documentation]                                             Admin Verification User Register
        Execute Javascript                                          window.open('https://admin-smartchange.deggan.com/login')
        Switch Window                                               locator=New
        Wait Until Element Is Visible                               id=login-email                                                                      10
        Input Text                                                  id=login-email                                                                      admin@smartchange.com
        Input Password                                              id=login-password                                                                   password
        Click Element                                               //button[contains(text(),'Log In')]
        Wait Until Element Is Visible                               (//a[normalize-space()='Lecturer'])[1]                                              10
        Click Element                                               (//a[normalize-space()='Lecturer'])[1]
        Wait Until Element Is Visible                               (//tr[@role='row'])[2]                                                              10
        Click Element                                               (//button[@type='submit'][normalize-space()='Verfiy'])[1]
        Capture Page Screenshot                                     ${path}dataRegisterUserLecturer.png
        Click Element                                               (//button[normalize-space()='Accept'])[1]
        Click Element                                               (//button[normalize-space()="Yes, I'm Sure"])[1]
        Wait Until Element Is Visible                               (//div[@id='swal2-html-container'])[1]                                              10
        Page Should Contain Button                                  (//button[normalize-space()='OK'])[1]
        Sleep    1s
        Capture Page Screenshot                                     ${path}successVerifyUserLecturer.png
        Click Element                                               (//button[normalize-space()='OK'])[1]                                 
        Sleep   3s
        Close Browser