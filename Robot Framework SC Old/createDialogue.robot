*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${path}    D:\\Work\\Deggan\\Automation\\automation-testing\\SmartChange\\Screenshot\\

*** Test Cases ***
Create New Dialogue
        [Documentation]    User Create New Dialogue on Smart Change
        Open Browser    https://smartchange.deggan.com/    chrome
        Register Keyword To Run On Failure    NONE
        Maximize Browser Window
        Wait Until Element Is Visible    (//button[normalize-space()='Login'])[1]    10
        Click Element    (//button[normalize-space()='Login'])[1]
        Wait Until Element Is Visible    (//input[@placeholder='john@example.com'])[1]    10
        Input Text    (//input[@placeholder='john@example.com'])[1]    asd@gmail.com
        Input Password    (//input[@placeholder='············'])[1]    1
        Checkbox Should Not Be Selected    (//input[@id='remember-me'])[1]
        Click Button    (//button[normalize-space()='LOGIN'])[1]
        Sleep    1s
        Click Element    (//span[normalize-space()='Dialogues'])[1]
        Wait Until Page Contains Element    (//div[@class='row row-cols-1 no-gutters pt-3'])[1]
        Capture Page Screenshot    ${path}dialoguePage.png
        Wait Until Element Is Visible    (//a[normalize-space()='Create New Dialogue'])[1]    10
        Click Element    (//a[normalize-space()='Create New Dialogue'])[1]
        Wait Until Element Is Visible    (//input[@placeholder='Title'])[1]    10
        Input Text    (//input[@placeholder='Title'])[1]    Create Dialogue Using Robot Framework
        Input Text    (//input[@placeholder='Tags'])[1]    mencoba
        Press Keys    (//input[@placeholder='Tags'])[1]    ENTER
        Wait Until Element Is Visible    (//div[@aria-label='Rich Text Editor, main'])[1]    10
        Input Text    (//div[@aria-label='Rich Text Editor, main'])[1]    DDescription Create New Dialogue Using Robot Framework
        Capture Page Screenshot    ${path}createNewDialogue.png
        Sleep    1s
        Click Button    (//button[normalize-space()='Save'])[1]
        Sleep    1s
        Capture Page Screenshot    ${path}successCreateDialogue.png
        Wait Until Location Is    https://smartchange.deggan.com/dialogues/mine    10
        Sleep    1s
        Capture Page Screenshot    ${path}listDialogue.png
        Close Browser
