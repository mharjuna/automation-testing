*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${fileImg}    D:\\Work\\Deggan\\Automation\\automation-testing\\DummyDocs\\lorem-ipsum.png
${path}    D:\\Work\\Deggan\\Automation\\automation-testing\\SmartChange\\Screenshot\\

*** Test Cases ***
Moderation Profile
        [Documentation]    Moderation Profile User Smart Change
        Open Browser    https://smartchange.deggan.com/    chrome
        Register Keyword To Run On Failure    NONE
        Maximize Browser Window
        Wait Until Element Is Visible    (//button[normalize-space()='Login'])[1]    10
        Click Element    (//button[normalize-space()='Login'])[1]
        Wait Until Element Is Visible    (//input[@placeholder='john@example.com'])[1]    10
        Input Text    (//input[@placeholder='john@example.com'])[1]    asd@gmail.com
        Input Password    (//input[@placeholder='············'])[1]    1
        Click Button    (//button[normalize-space()='LOGIN'])[1]
        Wait Until Element Is Visible    (//i[@class='far fa-user-circle profile-wrapper'])[1]    10
        Click Element    (//i[@class='far fa-user-circle profile-wrapper'])[1]
        Wait Until Element Is Visible    (//li[@class='user-menu__item'])[1]    10
        Click Element    (//li[@class='user-menu__item'])[1]
        Sleep    2s
        Double Click Element    (//a[normalize-space()='Networking'])[1]
        Input Text    (//input[@placeholder='Find people or industries...'])[1]    testing
        Wait Until Element Contains    (//h6[normalize-space()='testing'])[1]    testing
        Capture Page Screenshot    ${path}searchUserToReport.png
        Click Element    (//h6[normalize-space()='testing'])[1]
        Wait Until Element Contains    (//h6[normalize-space()='testing'])[1]    testing
        Wait Until Element Is Visible    (//button[normalize-space()='Report'])[1]    10
        Capture Page Screenshot    ${path}profileUserToReport.png
        Click Button    (//button[normalize-space()='Report'])[1]   
        Wait Until Element Is Visible    (//div[@class='p-4'])[1]    10
        Click Element    (//label[normalize-space()='Inappropriate profile'])[1]
        Input Text    (//textarea)[1]    Report User Profile Using Robot Framework
        Choose File    (//input[@type='file'])[1]    ${fileImg}
        Capture Page Screenshot    ${path}formReportUser.png
        Click Button    (//button[normalize-space()='Submit'])[1]
        Wait Until Element Contains    (//div[@id='swal2-html-container'])[1]    Success to Submit Report User
        Sleep    1s
        Capture Page Screenshot    ${path}successReportUser.png
        Click Button    (//button[normalize-space()='OK'])[1]

Admin Verify Report
        [Documentation]    Admin Choose to Close Case (Not Banning)
        Execute Javascript    window.open('https://admin-smartchange.deggan.com/login')
        Sleep    1s
        Switch Window    locator=New
        Wait Until Element Is Visible    id=login-email    10
        Input Text    id=login-email    admin@smartchange.com
        Input Password    id=login-password    password
        Click Element    //button[contains(text(),'Log In')]
        Wait Until Element Is Visible    (//a[normalize-space()='Moderation'])[1]    10
        Click Element    (//a[normalize-space()='Moderation'])[1]
        Sleep    1s
        Click Element    (//a[normalize-space()='Profile'])[1]
        Wait Until Element Is Visible    (//tr[@role='row'])[7]    10
        Capture Element Screenshot    (//tr[@role='row'])[7]    ${path}listUserReport.png
        Click Element    (//a[contains(text(),'Detail')])[6]
        Sleep    1s
        Capture Page Screenshot    ${path}listBanningUser.png
        Click Button    (//button[normalize-space()='Action'])[1]
        Wait Until Element Is Visible    (//span[normalize-space()='Close case (not banning)'])[1]    10
        Click Element    (//span[normalize-space()='Close case (not banning)'])[1]
        Capture Page Screenshot    ${path}adminChooseToCloseCase.png
        Sleep    1s
        Click Button    (//button[normalize-space()='Submit'])[1]
        Wait Until Element Contains    (//h2[normalize-space()='Are you sure to submit?'])[1]    Are you sure to submit?
        Click Button    (//button[normalize-space()='Yes, accept it!'])[1]
        Sleep    1s
        Capture Page Screenshot    ${path}successToCloseCase.png
        Close Browser