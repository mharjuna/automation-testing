**** Settings ***
Library                                                         SeleniumLibrary

*** Variables ***
${filePdf}                                                      D:\\Work\\Deggan\\Automation\\automation-testing\\DummyDocs\\dummies.pdf
${fileImg}                                                      D:\\Work\\Deggan\\Automation\\automation-testing\\DummyDocs\\lorem-ipsum.png
${path}                                                         D:\\Work\\Deggan\\Automation\\automation-testing\\SmartChange\\Screenshot\\

*** Test Cases ***
Create New Online Learning
        Open Browser                                            https://admin-smartchange.deggan.com/login                                              Chrome
        Register Keyword To Run On Failure                      NONE
        Maximize Browser Window
        Wait Until Element Is Visible                           id=login-email                                                                          10
        Input Text                                              id=login-email                                                                          admin@smartchange.com
        Input Password                                          id=login-password                                                                       password
        Click Element                                           //button[contains(text(),'Log In')]
        Wait Until Element Is Visible                           //a[contains(text(),'Online Learning')]                                                 10
        Click Element                                           //a[contains(text(),'Online Learning')]                               
        Wait Until Element Is Visible                           (//a[normalize-space()='Course'])[1]                                                    10
        Click Element                                           (//a[normalize-space()='Course'])[1]
        Wait Until Element Is Visible                           (//small[normalize-space()='Add New'])[1]                                               10
        Click Element                                           (//small[normalize-space()='Add New'])[1]
        Wait Until Element Is Visible                           //*[@id="__BVID__102"]                                                                  10
        Input Text                                              //*[@id="__BVID__102"]                                                                  Create New Online Learning Using Robot Framework
        Input Text                                              (//input[@type='search'])[1]                                                            Prog
        Press Keys                                              (//div[@class='vs__selected-options'])[1]                                               ENTER
        Input Text                                              //div[@id='vs2__combobox']//input[@type='search']                                       admin
        Press Keys                                              (//div[@class='vs__selected-options'])[2]                                               ENTER
        Click Element                                           (//button[@type='button'][normalize-space()='+ Add Lecturer'])[1]
        Wait Until Element Is Visible                           (//input[@type='search'])[3]                                                            10
        Input Text                                              (//input[@type='search'])[3]                                                            Fred
        Press Keys                                              (//div[@class='vs__selected-options'])[3]                                               ENTER
        Select From List By Value                               //*[@id="__BVID__119"]                                                                  false
        Click Element                                           (//input[@placeholder='Date'])[1]
        Wait Until Element Is Visible                           (//span[contains(text(),'30')])[1]                                                      10
        Click Element                                           (//span[contains(text(),'30')])[1]
        Sleep    1s
        Click Element                                           (//input[@placeholder='Time'])[1]
        Wait Until Element Is Visible                           (//li[contains(text(),'01')])[1]                                                        10
        Click Element                                           (//li[contains(text(),'01')])[1]
        Sleep    1s
        Click Element                                           (//li[contains(text(),'00')])[2]
        Sleep    1s
        Wait Until Element Is Visible                           (//input[@placeholder='Date'])[1]                                                       10
        Double Click Element                                    (//input[@placeholder='Date'])[1]
        Sleep    1s
        Click Element                                           (//input[@placeholder='Date'])[2]
        Wait Until Element Is Visible                           (//span[contains(text(),'>')])[4]                                                       10
        Click Element                                           (//span[contains(text(),'>')])[4]
        Click Element                                           (//span[@class='cell day'][normalize-space()='31'])[1]
        Sleep    1s
        Click Element                                           (//input[@class='display-time form-control is-empty'])[1]
        Wait Until Element Is Visible                           (//li[contains(text(),'23')])[3]                                                        10
        Click Element                                           (//li[contains(text(),'23')])[3]
        Click Element                                           (//li[contains(text(),'00')])[4]
        Sleep    1s
        Double Click Element                                    (//input[@placeholder='Date'])[2]
        Click Element                                           (//input[@placeholder='Date'])[3]
        Wait Until Element Is Visible                           (//span[contains(text(),'>')])[7]                                                       10
        Double Click Element                                    (//span[contains(text(),'>')])[7]
        Wait Until Element Is Visible                           (//span[contains(text(),'29')])[7]                                                      10
        Click Element                                           (//span[contains(text(),'29')])[7]
        Click Element                                           (//input[@placeholder='Time'])[3]
        Wait Until Element Is Visible                           (//li[contains(text(),'00')])[5]                                                        10
        Click Element                                           (//li[contains(text(),'00')])[5]
        Sleep    1s
        Click Element                                           (//li[contains(text(),'29')])[3]
        Double Click Element                                    (//input[@placeholder='Date'])[3]
        Input Text                                              (//p)[2]                                                                                DDescription Testing Create New Online Learning Using Robot Framework
        Choose File                                             (//input[@type='file'])[2]                                                              ${filePdf}
        Input Text                                              //*[@id="__BVID__141"]                                                                  https://www.youtube.com/watch?v=Wch3gJG2GJ4
        Choose File                                             (//input[@accept='.jpg,.png,.jpeg'])[1]                                                 ${fileImg}
        Scroll Element Into View                                (//button[normalize-space()='ID'])[1]
        Click Element                                           (//button[normalize-space()='ID'])[1]
        Wait Until Element Is Visible                           //*[@id="__BVID__146"]                                                                  10
        Input Text                                              //*[@id="__BVID__146"]                                                                  Mencoba Membuat Kursus Online Menggunakan Robot Framework
        Wait Until Element Is Enabled                           (//div[@aria-label='Rich Text Editor, main'])[2]                                        10
        Input Text                                              (//div[@aria-label='Rich Text Editor, main'])[2]                                        DDeskripsi Membuat Kursus Online Menggunakan Robot Framework
        Click Element                                           (//button[normalize-space()='Next'])[1]
        Wait Until Element Is Visible                           //*[@id="__BVID__153"]                                                                  10
        Input Text                                              //*[@id="__BVID__153"]                                                                  Episode 1
        Input Text                                              (//input[@type='search'])[4]                                                            Fred
        Press Keys                                              (//div[@class='vs__selected-options'])[4]                                               ENTER
        Choose File                                             (//input[@type='file'])[6]                                                              ${filePdf}
        Input Text                                              //*[@id="__BVID__166"]                                                                  https://www.youtube.com/watch?v=Wch3gJG2GJ4
        Input Text                                              (//div[@aria-label='Rich Text Editor, main'])[3]                                        DDescription Episode 1
        Click Element                                           (//button[normalize-space()='+ Add Section'])[1]
        Wait Until Element Is Visible                           //*[@id="__BVID__225"]                                                                  10
        Input Text                                              //*[@id="__BVID__225"]                                                                  Episode 2
        Input Text                                              (//input[@type='search'])[5]                                                            Admin
        Press Keys                                              (//div[@class='vs__selected-options'])[5]                                               ENTER
        Choose File                                             (//input[@type='file'])[9]                                                              ${filePdf}
        Input Text                                              //*[@id="__BVID__244"]                                                                  https://www.youtube.com/watch?v=Wch3gJG2GJ4
        Input Text                                              (//div[@aria-label='Rich Text Editor, main'])[4]                                        DDescription Episode 2
        Click Button                                            (//button[normalize-space()='Submit'])[1]
        Wait Until Element Contains                             (//div[@id='swal2-html-container'])[1]                                                  Successfuly added course
        Sleep    1s
        Capture Page Screenshot                                 ${path}successCreateOnlineLearning.png

Verify Online Learning
        [Documentation]    Verify Online Learning Is Available on Website Smart Change
        Execute Javascript    window.open('https://smartchange.deggan.com/')
        Sleep    1s
        Switch Window    locator=New
        Wait Until Element Is Visible    (//span[@class='menu-text'][normalize-space()='Online Learning'])[1]    100
        Click Element    (//span[@class='menu-text'][normalize-space()='Online Learning'])[1]
        Page Should Contain    Create New Online Learning Using Robot Framework
        Wait Until Element Is Visible    (//button[normalize-space()='Sign In to Become a Lecturer'])[1]    10
        Scroll Element Into View    (//button[normalize-space()='Sign In to Become a Lecturer'])[1]
        Sleep    3s
        Capture Page Screenshot    ${path}theOnlineLearningAvailable.png
        Click Element    (//img[@class='img-cover'])[1]
        Sleep    3s
        Capture Page Screenshot    ${path}resultNewOnlineLearning.png
        Close Browser        
