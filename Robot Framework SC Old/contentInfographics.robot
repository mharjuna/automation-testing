*** Settings ****
Library                                             SeleniumLibrary

*** Variables ***
${fileImg}                                          D:\\Work\\Deggan\\Automation\\automation-testing\\DummyDocs\\lorem-ipsum.png
${path}                                             D:\\Work\\Deggan\\Automation\\automation-testing\\SmartChange\\Screenshot\\

*** Test Cases ***
Create Content Infographics
        [Documentation]                             Create New Content Infographics on Smart Change
        Open Browser                                https://admin-smartchange.deggan.com/login                              Edge
        Register Keyword To Run On Failure          NONE
        Maximize Browser Window
        Wait Until Element Is Enabled               id=login-email                                                          10
        Input Text                                  id=login-email                                                          admin@smartchange.com
        Input Password                              id=login-password                                                       password
        Click Element                               //button[contains(text(),'Log In')]
        Wait Until Element Is Visible               //a[contains(text(),'Content')]                                         10
        Click Element                               //a[contains(text(),'Content')]
        Wait Until Element Is Visible               (//a[normalize-space()='Infographics'])[1]                              10
        Click Element                               (//a[normalize-space()='Infographics'])[1]
        Wait Until Element Is Visible               (//small[normalize-space()='Add New'])[1]                               10
        Click Element                               (//small[normalize-space()='Add New'])[1]
        Wait Until Element Is Visible               //*[@id="__BVID__73"]                                                   10
        Input Text                                  //*[@id="__BVID__73"]                                                   Create New Content Infographics Using Robot Framework
        Input Text                                  (//input[@placeholder='Tags'])[1]                                       Testing
        Input Text                                  //*[@id="__BVID__78"]                                                   Infographics Caption 1
        Choose File                                 //input[@type='file']                                                   ${fileImg}
        Click Element                               (//button[normalize-space()='+ Add New'])[1]
        Wait Until Element Is Visible               //*[@id="__BVID__92"]                                                   10
        Input Text                                  //*[@id="__BVID__92"]                                                   Infographics Caption 2
        Choose File                                 (//input[@type='file'])[2]                                              ${fileImg}
        Capture Page Screenshot                     ${path}formInfographicsEN.png
        Click Element                               (//button[normalize-space()='ID'])[1]
        Wait Until Element Is Visible               //*[@id="__BVID__81"]                                                   10
        Input Text                                  //*[@id="__BVID__81"]                                                   Membuat Konten Infographics Baru Menggunakan Robot Framework
        Input Text                                  //*[@id="__BVID__84"]                                                   Infographics Caption Satu
        Input Text                                  //*[@id="__BVID__98"]                                                   Infographics Caption Dua
        Capture Page Screenshot                     ${path}formInfographicsID.png
        Click Button                                (//button[normalize-space()='Submit'])[1]
        Wait Until Element Contains                 (//div[@id='swal2-html-container'])[1]                                  Successfuly added infographics
        Sleep                                       3s
        Capture Page Screenshot                     ${path}successCreateInfographics.png

Verify Infographics Is Available
        [Documentation]    This Robot Verify Infographics Is Available in Content Smart Change
        Execute Javascript    window.open('https://smartchange.deggan.com/')
        Sleep    1s
        Switch Window    locator=New
        Wait Until Element Is Visible    (//span[normalize-space()='Contents'])[1]    10
        Click Element    (//span[normalize-space()='Contents'])[1]
        Wait Until Element Is Visible    (//li)[6]    10
        Click Element    (//li)[6]
        Mouse Over    (//span[@class='menu-text'][normalize-space()='Online Learning'])[1]
        Page Should Contain Element    (//a[normalize-space()='Create New Content Infographic...'])[1]
        Sleep    3s
        Capture Page Screenshot    ${path}theInfographicsAvailable.png
        Wait Until Element Is Visible    (//img)[3]    10
        Click Element    (//img)[3]
        Sleep    1s
        Capture Page Screenshot    ${path}resultContentInfographics.png
        Close Browser