*** Settings ***
Library    SeleniumLibrary


*** Variables ***
${path}    D:\\Work\\Deggan\\Automation\\automation-testing\\SmartChange\\Screenshot\\


*** Test Cases ***
Report Discussion Online Learning
        [Documentation]    User Report Anonther Discussion User in Online Learning Smart Change
        Open Browser    https://smartchange.deggan.com/    Chrome
        Register Keyword To Run On Failure    NONE
        Maximize Browser Window
        Wait Until Element Is Visible    (//button[normalize-space()='Login'])[1]    10
        Click Element    (//button[normalize-space()='Login'])[1]
        Wait Until Element Is Visible    (//input[@placeholder='john@example.com'])[1]    10
        Input Text    (//input[@placeholder='john@example.com'])[1]    asd@gmail.com
        Input Password    (//input[@placeholder='············'])[1]    1
        Click Button    (//button[normalize-space()='LOGIN'])[1]
        Sleep    1s
        Wait Until Element Is Visible    (//span[@class='menu-text'][normalize-space()='Online Learning'])[1]    10
        Click Element    (//span[@class='menu-text'][normalize-space()='Online Learning'])[1]
        Sleep    3s
        Wait Until Element Is Visible    (//img)[19]    10
        Scroll Element Into View    (//a[normalize-space()='Prev'])[1]
        Click Element    (//img)[19]
        Wait Until Element Is Visible    (//a[normalize-space()='Disscussion'])[1]    10
        Click Element    (//a[normalize-space()='Disscussion'])[1]
        Wait Until Element Is Visible    (//button[normalize-space()='Send'])[1]    10
        Scroll Element Into View    (//button[normalize-space()='Send'])[1]
        Capture Page Screenshot    ${path}discussionUser.png
        Click Element    (//img[@class='ml-2'])[2]
        Page Should Contain    Are you sure you want to report this comment?
        Click Button    (//button[normalize-space()="Yes, I'm Sure"])[1]
        Sleep    1s
        Page Should Contain    Successfully reported this comment
        Capture Page Screenshot    ${path}successReportDiscussion.png
        Click Button    (//button[normalize-space()='OK'])[1]

Verify Report Disuccsion
        [Documentation]    Admin Verify Report Discussion User
        Execute Javascript    window.open('https://admin-smartchange.deggan.com/login')
        Sleep    1s
        Switch Window    locator=New
        Wait Until Element Is Visible    id=login-email    10
        Input Text    id=login-email    admin@smartchange.com
        Input Password    id=login-password    password
        Click Element    //button[contains(text(),'Log In')]
        Wait Until Element Is Visible    (//a[normalize-space()='Moderation'])[1]    10
        Click Element    (//a[normalize-space()='Moderation'])[1]
        Sleep    1s
        Click Element    (//a[normalize-space()='Discussion'])[1]
        Wait Until Element Is Visible    (//tr[@role='row'])[2]    10
        Click Element    //tbody/tr[1]/td[5]/a[1]
        Sleep    1s
        Capture Page Screenshot    ${path}discussionReportReview.png
        Wait Until Element Is Visible    (//button[normalize-space()='Delete'])[1]    10
        Click Button    (//button[normalize-space()='Delete'])[1]
        Page Should Contain Button    (//button[normalize-space()='Yes, delete it!'])[1]
        Click Button    (//button[normalize-space()='Yes, delete it!'])[1]
        Page Should Contain Element   (//div[@id='swal2-html-container'])[1]
        Sleep    1s
        Capture Page Screenshot    ${path}successDeleteDisucssion.png
        Sleep    3s
        Switch Window    locator=Main
        Sleep    3s
        Reload Page
        Wait Until Element Is Enabled    (//a[normalize-space()='Disscussion'])[1]    100
        Click Element    (//a[normalize-space()='Disscussion'])[1]
        Wait Until Element Is Visible    (//button[normalize-space()='Send'])[1]    10
        Scroll Element Into View    (//button[normalize-space()='Send'])[1]
        Sleep    1s
        Capture Page Screenshot    ${path}resultDeleteDisuccion.png
        Close Browser