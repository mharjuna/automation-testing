*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${path}    D:\\Work\\Deggan\\Automation\\automation-testing\\SmartChange\\Screenshot\\
${fileImg}    D:\\Work\\Deggan\\Automation\\automation-testing\\DummyDocs\\lorem-ipsum.png


*** Tasks ***
Moderation Message
        [Documentation]    User do reporting Message another User on Website Smart Change
        Open Browser    https://smartchange.co.id/    Chrome
        Register Keyword To Run On Failure    NONE
        Maximize Browser Window
        Wait Until Element Is Visible    (//button[normalize-space()='Login'])[1]    10
        Click Element    (//button[normalize-space()='Login'])[1]
        Wait Until Element Is Visible    (//input[@placeholder='john@example.com'])[1]    10
        Input Text    (//input[@placeholder='john@example.com'])[1]    asd@gmail.com
        Input Password    (//input[@placeholder='············'])[1]    1
        Click Button    (//button[normalize-space()='LOGIN'])[1]
        Wait Until Element Is Visible    (//i[@class='far fa-user-circle profile-wrapper'])[1]    10
        Click Element    (//i[@class='far fa-user-circle profile-wrapper'])[1]
        Wait Until Element Is Visible    (//li[@class='user-menu__item'])[1]    10
        Click Element    (//li[@class='user-menu__item'])[1]
        Sleep    2s
        Double Click Element    (//a[normalize-space()='Networking'])[1]
        Input Text    (//input[@placeholder='Find people or industries...'])[1]    testing
        Wait Until Element Contains    (//h6[normalize-space()='testing'])[1]    testing
        Click Element    (//h6[normalize-space()='testing'])[1]
        Wait Until Element Contains    (//h6[normalize-space()='testing'])[1]    testing
        Wait Until Element Is Visible    (//a[normalize-space()='Send Message'])[1]    10
        Click Element    (//a[normalize-space()='Send Message'])[1]
        Wait Until Element Is Visible    (//span[@class='d-none d-lg-block'])[1]    10
        Scroll Element Into View    (//span[@class='d-none d-lg-block'])[1]
        Input Text    (//input[@placeholder='Type your message'])[1]    Testing Send Message Using Robot Framework
        Click Element    (//span[@class='d-none d-lg-block'])[1]
        Sleep    1s
        Capture Page Screenshot    ${path}sendMessage.png
        Click Button    (//button[normalize-space()='Report'])[1]
        Page Should Contain    Report this message?
        Click Element    (//label[normalize-space()='Inappropriate content'])[1]
        Input Text    (//textarea)[1]    Description Report Message User Using Robot Framework
        Choose File    (//input[@type='file'])[1]    ${fileImg}
        Capture Page Screenshot    ${path}formReportMessage.png
        Click Button    (//button[normalize-space()='Submit'])[1]
        Page Should Contain    Success to Submit Report User
        Sleep    1s
        Capture Page Screenshot    ${path}successReportMessage.png
        Click Button    (//button[normalize-space()='OK'])[1]
        Sleep    1s

Verify Report
        [Documentation]    Admin Verify Report Message User
        Execute Javascript    window.open('https://admin-smartchange.deggan.com/login')
        Sleep    1s
        Switch Window    locator=New
        Wait Until Element Is Visible    id=login-email    10
        Input Text    id=login-email    admin@smartchange.com
        Input Password    id=login-password    password
        Click Element    //button[contains(text(),'Log In')]
        Wait Until Element Is Visible    (//a[normalize-space()='Moderation'])[1]    10
        Click Element    (//a[normalize-space()='Moderation'])[1]
        Sleep    1s
        Click Element    (//a[normalize-space()='Message'])[1]
        Wait Until Element Is Visible    (//tr[@role='row'])[6]    10
        Click Element    (//a[contains(text(),'Detail')])[5]
        Capture Page Screenshot    ${path}dataReportUser.png
        Wait Until Element Is Visible    (//tr[@role='row'])[2]    10
        Click Element    (//a[contains(text(),'Review')])[1] 
        Sleep    3s
        Capture Page Screenshot    ${path}resultReportMessage.png
        Click Element    (//a[normalize-space()='Back'])[1]
        Wait Until Element Is Visible    (//button[normalize-space()='Action'])[1]    10
        Click Element    (//button[normalize-space()='Action'])[1]
        Page Should Contain    Actions Taken ?
        Click Element    (//label[@for='radio-group-1_BV_option_2'])[1]
        Capture Page Screenshot    ${path}adminChooseCloseCase.png
        Click Element    (//button[normalize-space()='Submit'])[1]
        Page Should Contain Button    (//button[normalize-space()='Yes, accept it!'])[1]
        Click Button    (//button[normalize-space()='Yes, accept it!'])[1]
        Sleep    1s
        Capture Page Screenshot    ${path}successVerifyReportMessage.png
        Close Browser