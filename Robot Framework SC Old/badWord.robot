*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${path}    D:\\Work\\Deggan\\Automation\\automation-testing\\SmartChange\\Screenshot\\


*** Test Cases ***
Bad Word
        [Documentation]    Testing & Checking Bad Word in Website Smart Change
        Open Browser    https://smartchange.deggan.com/    Chrome
        Register Keyword To Run On Failure    NONE
        Maximize Browser Window
        Wait Until Element Is Visible    (//button[normalize-space()='Login'])[1]    10
        Click Element    (//button[normalize-space()='Login'])[1]
        Wait Until Element Is Visible    (//input[@placeholder='john@example.com'])[1]    10
        Input Text    (//input[@placeholder='john@example.com'])[1]    asd@gmail.com
        Input Password    (//input[@placeholder='············'])[1]    1
        Click Button    (//button[normalize-space()='LOGIN'])[1]
        Wait Until Element Is Visible    (//i[@class='far fa-user-circle profile-wrapper'])[1]    10
        Click Element    (//i[@class='far fa-user-circle profile-wrapper'])[1]
        Wait Until Element Is Visible    (//li[@class='user-menu__item'])[1]    10
        Click Element    (//li[@class='user-menu__item'])[1]
        Sleep    2s
        Double Click Element    (//a[normalize-space()='Networking'])[1]
        Input Text    (//input[@placeholder='Find people or industries...'])[1]    testing
        Wait Until Element Contains    (//h6[normalize-space()='testing'])[1]    testing
        Click Element    (//h6[normalize-space()='testing'])[1]
        Wait Until Element Contains    (//h6[normalize-space()='testing'])[1]    testing
        Wait Until Element Is Visible    (//a[normalize-space()='Send Message'])[1]    10
        Click Element    (//a[normalize-space()='Send Message'])[1]
        Wait Until Element Is Visible    (//span[@class='d-none d-lg-block'])[1]    10
        Scroll Element Into View    (//span[@class='d-none d-lg-block'])[1]
        Input Text    (//input[@placeholder='Type your message'])[1]    Spongebob
        Click Element    (//span[@class='d-none d-lg-block'])[1]
        Sleep    1s
        Reload Page
        Sleep    2s
        Capture Page Screenshot    ${path}beforeAddingBadWord.png

Input The Bad Word
        [Documentation]    Admin Adding a Bad Word to Website Smart Change
        Execute Javascript    window.open('https://admin-smartchange.deggan.com/login')
        Sleep    1s
        Switch Window    locator=New
        Wait Until Element Is Visible    id=login-email    10
        Input Text    id=login-email    admin@smartchange.com
        Input Password    id=login-password    password
        Click Element    //button[contains(text(),'Log In')]
        Wait Until Element Is Visible    (//a[normalize-space()='Settings'])[1]    10
        Click Element    (//a[normalize-space()='Settings'])[1]
        Wait Until Element Is Visible    (//a[normalize-space()='Bad Word'])[1]    10
        Click Element    (//a[normalize-space()='Bad Word'])[1]
        Wait Until Element Is Visible    (//small[normalize-space()='Add New'])[1]     10
        Click Element    (//small[normalize-space()='Add New'])[1]
        Wait Until Element Is Visible    //*[@id="__BVID__77"]    10
        Input Text    //*[@id="__BVID__77"]    Spongebob
        Sleep    1s
        Capture Page Screenshot    ${path}addBadWord.png
        Click Button    (//button[normalize-space()='Submit'])[1]
        Sleep    3s
        Page Should Contain    Successfuly added badword 
        Sleep    1s
        Capture Page Screenshot    ${path}successAddBadWord.png
        Click Button    (//button[normalize-space()='OK'])[1]
        Sleep    3s
        Switch Window    locator=Main
        Reload Page
        Sleep    3s
        Capture Page Screenshot    ${path}afterAddNewBadWord.png
        Close Browser
